<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.brown-forman.com
 * @since             1.0.0
 * @package           Bf_Product_Locator
 *
 * @wordpress-plugin
 * Plugin Name:       BF Product Locator
 * Plugin URI:        https://www.brown-forman.com
 * Description:       BF Product Locator Plugin. [bf-product-locator]. See README for instructions and attributes.
 * Version:           1.0.0
 * Author:            Brown-Forman
 * Author URI:        https://www.brown-forman.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bf-product-locator
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently pligin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'BF_PRODUCT_LOCATOR_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-bf-product-locator-activator.php
 */
function activate_bf_product_locator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bf-product-locator-activator.php';
	Bf_Product_Locator_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-bf-product-locator-deactivator.php
 */
function deactivate_bf_product_locator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bf-product-locator-deactivator.php';
	Bf_Product_Locator_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_bf_product_locator' );
register_deactivation_hook( __FILE__, 'deactivate_bf_product_locator' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-bf-product-locator.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_bf_product_locator() {

	$plugin = new Bf_Product_Locator();
	$plugin->run();

}
run_bf_product_locator();
