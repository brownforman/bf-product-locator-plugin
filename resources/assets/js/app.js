(function($) {
	let bfploc_map, autocomplete;
	//let distance_system = 'Kilometers';
	let distance_system = bfploc.distance_system;
	let geocoder = infoWindow = null;
	let bfploc_markers = [];
	let search_dirty = false;
	let api_url = bfploc.bf_api_url;

	function bfploc_setup_autocomplete(){
		autocomplete = new google.maps.places.Autocomplete($('#search_string')[0]);
		$('#search_string').on('change', function(){
			search_dirty = true;
		});

		autocomplete.addListener('place_changed', function(){
			search_dirty = false;
		});
	}

	function bfploc_decimal_adjust(type, value, exp) {
		// If the exp is undefined or zero...
		if (typeof exp === 'undefined' || +exp === 0) {
			return Math[type](value);
		}
		value = +value;
		exp = +exp;
		// If the value is not a number or the exp is not an integer...
		if (value === null || isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
			return NaN;
		}
		// If the value is negative...
		if (value < 0) {
			return -bfploc_decimal_adjust(type, -value, exp);
		}
		// Shift
		value = value.toString().split('e');
		value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
		// Shift back
		value = value.toString().split('e');
		return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	}

	function bfploc_clear_markers() {
		infoWindow.close();
		bfploc_markers.forEach(function(marker){
			marker[0].setMap(null);
		});
		bfploc_markers = [];
	}

	function bfploc_marker_click(marker, html) {
		infoWindow.setContent(html);
		infoWindow.open(bfploc_map, marker);
		bfploc_map.panTo(marker.getPosition());
	}

	function bfploc_create_marker(value) {
		let marker = new google.maps.Marker({
			position: new google.maps.LatLng( value.latitude, value.longitude ),
			map: bfploc_map,
			title: value.company
		});

		let html = 	`
			<div class="bfploc-infoWindow">
				<span class="bfploc-result--company">${value.company}</span>
				<span class="bfploc-result--distance">${value.distance} miles</span>
				<span class="bfploc-result--address">${value.address}</span>
				<span class="bfploc-result--city">${value.city}</span>
				<span class="bfploc-result--state">${value.state}</span>
				<span class="bfploc-result--postal_code">${value.postal_code}</span>
			</div>
		`;

		google.maps.event.addListener(marker, "click", function() {
			bfploc_marker_click(marker, html);
		});

		bfploc_markers.push([marker, html]);
	}

	function bfploc_init_map(){
		let map_center = bfploc.map_center.split(',');
		map_center = {lat: parseFloat(map_center[0].trim()), lng:  parseFloat(map_center[1].trim())};
        bfploc_map = new google.maps.Map(document.getElementById('bfploc_map'), {
          center: map_center,
          zoom: parseInt(bfploc.map_zoom)
        });

        geocoder = new google.maps.Geocoder();
        infoWindow = new google.maps.InfoWindow();
	}

	function bfploc_set_minor_brands_dropdown(){
		if($('#bfploc_form #minor_brand_id').length < 1) {
			//minor brand drop down has been removed from the form
			return;
		}

		if($('#bfploc_form #minor_brand_id option').length > 1 || ($('#bfploc_form #minor_brand_id option').length == 1 && $('#bfploc_form #minor_brand_id option').first().val() !== "")) {
			//dev has customized the drop down so we should not replace it
			return;
		}

		$.ajax({
			url: api_url + 'brand/'+$('#brand_id').val(),
			success: function(data) {
				//set via shortcode
				let excluded_minor_brands = bfploc.excluded_minor_brands.split(',');
				let html = $('#bfploc_form #minor_brand_id').html();
				data.minorBrands.forEach(function(value){
					if(value.brand_minor_description && $.inArray(value.brand_minor_id, excluded_minor_brands) <= -1){
						html += `<option value="${value.brand_minor_id}">${value.brand_minor_description}</option>`;
					}
				});
				$('#bfploc_form #minor_brand_id').html(html);
			},
			error: function(err){
				alert('Error retrieving minor brands.');
			}
		});
	}

	function bfploc_geocode(str) {
		return $.ajax({
			url: api_url+'geocode',
			dataType: 'json',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify({'search_string':str}),
		});
	}

	function bfploc_do_action(form_data){
		$.ajax({
			url: api_url + 'retail_locations_by_coords',
			dataType: 'json',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify(form_data),
			success: function(data){
				let i = 0;

				bfploc_map.setCenter({lat: form_data['lat'],lng: form_data['long']});

				//values are set to be comfortable for miles
				switch(parseInt($('#radius').val())) {
					case 50:
						bfploc_map.setZoom(8);
						break;
					case 25:
						bfploc_map.setZoom(9);
						break;
					case 15:
						bfploc_map.setZoom(10);
						break;
					default:
						bfploc_map.setZoom(11);
				}

				bfploc_clear_markers();
				$('.bfploc-results').html('');
				let locs = [];

				data.forEach(function(value){
					if($.inArray(value.LocationID, locs) !== -1) {
						return;
					}

					locs.push(value.LocationID);

					let dist = value.distance;
					if( distance_system === 'Kilometers') {
						dist = Math.round10(dist * 1.60934, -1); // convert miles to kilometers
					}
					let html = `
						<li class="bfploc-result" data-result-id="${i}">
							<span class="bfploc-result--company">${value.company}</span>
							<span class="bfploc-result--distance">${dist} ${distance_system}</span>
							<span class="bfploc-result--address">${value.address}</span>
							<span class="bfploc-result--city">${value.city}</span>
							<span class="bfploc-result--state">${value.state}</span>
							<span class="bfploc-result--postal_code">${value.postal_code}</span>
						</li>
					`;

					$('.bfploc-results').append(html);
					bfploc_create_marker(value);

					i++;
				});

				$('.bfploc-results .bfploc-result').click(function(){
					let i = $(this).data('result-id');
					bfploc_marker_click(bfploc_markers[i][0],bfploc_markers[i][1]);
				});
			},
			error: function(err){
				alert('Error with search');
			}
		});
	}

	//function

	function bfploc_set_submit(){
		$('#bfploc_form').on('submit', function(e){
			e.preventDefault();
			let form_data = {
				'radius': $('#radius').val(),
				'brand_id': $('#brand_id').val(),
				//'type': $('input[name=type]:checked').val(),
				'type': $('#type').val(),
				'site_id': $('#site_id').val(),
				'promotion_id': $('#promotion_id').val(),
			}

			//remove if not using a minor brand id filter
			if($('#minor_brand_id').length > 0 && $('#minor_brand_id').val() !== "") {
				form_data['minor_brand_id'] = $('#minor_brand_id').val();
			}

			if( distance_system === 'Kilometers') {
				form_data['radius'] = Math.round10(form_data['radius'] * 1.60934, -1); // convert miles to kilometers
			}


			if(bfploc.autocomplete && autocomplete.getPlace() && !search_dirty) {
				form_data['lat'] = autocomplete.getPlace().geometry.location.lat();
				form_data['long'] = autocomplete.getPlace().geometry.location.lng();
				bfploc_do_action( form_data );
			} else {
				$.when(bfploc_geocode($('#search_string').val())).then(function(data){
					form_data['lat'] = data.lat;
					form_data['long'] = data.lng;
					bfploc_do_action( form_data );
				});
			}


		});
	}

	bfploc_set_minor_brands_dropdown();

	if(bfploc.autocomplete) {
		bfploc_setup_autocomplete();
	}

	bfploc_set_submit();
	bfploc_init_map();

	//Math stuff
	if (!Math.round10) {
		Math.round10 = function(value, exp) {
			return bfploc_decimal_adjust('round', value, exp);
		};
	}

})( jQuery );
