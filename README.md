# BF Product Locator Plugin
Contributors: Brown-Forman
Tags: product-locator

Updates 2-2-22:
-Node 14
-NPM 6.14.15
-Converted for Pantheon


Brown-Forman product locator plugin for Wordpress.

### Description
Brown-Forman product locator plugin for Wordpress.

This plugin replaces the previous plugin and implements the newest Brown-Forman API (as of 12/2017).

### Installation

Activate on your site from the plugins page.

Ask Brown-Forman for site_id and brand_id to use in the bf-product-locator short code.

Create a Google Maps API Key and set the value as a constant in wp-config.php: 
`define('GOOGLE_MAPS_API_KEY', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');`

Short Code: `[bf-product-locator]`

Parameters:
* brand_id: Get this value from BF (required).
* site_id: Get this value from BF (required).
* distance_system: string, 'Miles' or 'Kilometers' (optional, defaults to 'Miles').
* map_center: string, comma separated latitude, longitude pair (optional, defaults to Lynchburg, TN)
* map_zoom: integer value for the default map zoom when the page loads. Smaller is more zoomed out (optional, defaults to 11)
* excluded_minor_brands: string, comma separated list of minor brand ID's to exclude from the dropdown.
* promotion_id: optional promotion id to filter results down to locations participating in a promotion (get this from BF IT)

Short Code Example:

`[bf-product-locator brand_id=17 site_id=20 distance_system='Miles' map_center='38.2527, -85.7585']`

### Customization

Run 'yarn install' or 'npm install' in the directory to download front-end dev tools.

Run 'npm run watch' to monitor the directory for code changes, 'npm run dev' or 'npm run production' to prepare the code for launch.

Update the HTML in public/partials/bf-product-locator-public-display.php

Change minor brand list (list of products in the form) by adding options to the minor_brand_id select element. If the list of options has more than just an "All" option with an empty value, Javascript will detect the customization and will not overwrite it with the minor brand list from the server.

You can also remove the minor_brand_id element if you only want to search all, or if the brand only has one product.

Modify javascript and scss files in resources/assets/js and resources/assets/sass. Files will deploy to public/js and public/css.

### Frequently Asked Questions
1. Does the plugin support multi-site? Yes, however as of 12/07/2017 it will be up to the developer/users to provide the translations in the form of mo/po/pot files. These can go in the languages folder. I suggest using the loco translate to generate these, though there are plenty of other options.

2. Is the plugin location aware? Yes, if the site is on Cloudflare. The plugin will detect the user's country and compare it to a list of countries to determine whether Miles or Kilometers are correct for the user. Otherwise, the plugin can be configured to use miles or meters through the shortcode attributes, however once deployed it will always be in miles or meters until this code can be written.

A side effect of this is that an American user in France, for example, will see Kilometers rather than Miles.

### Screenshots
