<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.brown-forman.com
 * @since      1.0.0
 *
 * @package    Bf_Product_Locator
 * @subpackage Bf_Product_Locator/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bf_Product_Locator
 * @subpackage Bf_Product_Locator/includes
 * @author     Brown-Forman <jeff_gullett@b-f.com>
 */
class Bf_Product_Locator_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
