<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.brown-forman.com
 * @since      1.0.0
 *
 * @package    Bf_Product_Locator
 * @subpackage Bf_Product_Locator/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Bf_Product_Locator
 * @subpackage Bf_Product_Locator/public
 * @author     Brown-Forman <jeff_gullett@b-f.com>
 */
class Bf_Product_Locator_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	private $google_maps_api_key = GOOGLE_MAPS_API_KEY;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bf_Product_Locator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bf_Product_Locator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/app.css', array(), $this->version, 'all' );

	}

	public function register_shortcodes() {
		add_shortcode( 'bf-product-locator', array( $this, 'bf_product_locator_shortcode') );
	}


	public function bf_product_locator_shortcode($atts = array()){
		$this->register_scripts();

		$distance_system = !empty($atts['distance_system']) ? $atts['distance_system'] : 'Miles';
		$map_center = !empty($atts['map_center']) ? $atts['map_center'] : '35.2830,-86.3740';
		$map_zoom = !empty($atts['map_zoom']) ? $atts['map_zoom'] : '11';
		$autocomplete = !empty($atts['autocomplete']) && $atts['autocomplete']=="on";
		$premise = !empty($atts['premise_type']) ? $atts['premise_type'] : 'offpremise';

		if(defined('BF_USER_COUNTRY')) {
			//use the user's country to determine miles vs KM
			///https://en.wikipedia.org/wiki/Mile#International_mile
			if(in_array(strtoupper(BF_USER_COUNTRY), array('US','LR', 'GB', 'MM', 'AS', 'BS', 'BZ', 'VG', 'KY', 'DM', 'FK', 'GD', 'GU', 'MP', 'WS', 'LC', 'VC', 'SH', 'KN', 'TC', 'UM', 'VI'))) {
				$distance_system = 'Miles';
			} else {
				$distance_system = 'Kilometers';
			}
		}

		$excluded_minor_brands = '';

		if(!empty($atts['excluded_minor_brands'])){
			$excluded_minor_brands = $atts['excluded_minor_brands'];
			$excluded_minor_brands = str_replace(' ', '', $excluded_minor_brands);
		}

		$promotion_id = !empty($atts['promotion_id']) ? $atts['promotion_id'] : '';

		wp_localize_script($this->plugin_name, 'bfploc', array('distance_system'=> $distance_system, 'map_center' => $map_center, 'map_zoom' => $map_zoom, 'excluded_minor_brands'=>$excluded_minor_brands, 'autocomplete'=>$autocomplete, 'bf_api_url'=>BF_API_URL));

		$this->enqueue_scripts();
		$this->enqueue_styles();
		$brand_id = $atts['brand_id'];
		$site_id = $atts['site_id'];
		ob_start();
		require_once plugin_dir_path( __FILE__ ). 'partials/bf-product-locator-public-display.php';
		$form = ob_get_contents();
		ob_end_clean();
		return $form;
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function register_scripts() {
		wp_register_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/app.js', array( 'jquery' ), $this->version, true );
	}

	/**
	 * Enqueue the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/**
		 * An instance of this class should be passed to the run() function
		 * defined in Bf_Product_Locator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bf_Product_Locator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script($this->plugin_name.'-google-maps', 'https://maps.googleapis.com/maps/api/js?key='.$this->google_maps_api_key.'&libraries=places', null, null, true);
		wp_enqueue_script($this->plugin_name);

	}

}
