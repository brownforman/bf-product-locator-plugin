<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.brown-forman.com
 * @since      1.0.0
 *
 * @package    Bf_Product_Locator
 * @subpackage Bf_Product_Locator/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="bfploc-outer-container">
	<form id="bfploc_form" class="bfploc-form">
		<div class="bfploc-row">
			<div class="bfploc-row--label"><label for="search_string"><?php _e('Product', 'bf-product-locator'); ?></label></div>
			<div class="bfploc-row--field">
				<select name="minor_brand_id" id="minor_brand_id" class="bfploc-minor-brand-id">
					<option value=""><?php _e('All', 'bf-product-locator'); ?></option>
				</select>
			</div>
		</div>
		<div class="bfploc-row">
			<div class="bfploc-row--label"><label for="search_string"><?php _e('Find By', 'bf-product-locator'); ?></label></div>
			<div class="bfploc-row--field">
				<input type="text" id="search_string" placeholder="<?php _e('Enter a City/State, or  Zip code', 'bf-product-locator'); ?>" required aria-required="true">
			</div>
		</div>
		<div class="bfploc-row">
			<div class="bfploc-row--label"><label for="radius"><?php _e('Distance', 'bf-product-locator'); ?></label></div>
			<div class="bfploc-row--field">
				<select name="radius" id="radius" required aria-required="true">
					<option value="5">5 <?php _e($distance_system, 'bf-product-locator'); ?></option>
					<option value="10" selected="selected">10 <?php _e($distance_system, 'bf-product-locator'); ?></option>
					<option value="15">15 <?php _e($distance_system, 'bf-product-locator'); ?></option>
					<option value="25">25 <?php _e($distance_system, 'bf-product-locator'); ?></option>
					<option value="50">50 <?php _e($distance_system, 'bf-product-locator'); ?></option>
				</select>
			</div>
		</div>
		<input type="hidden" id="brand_id" name="brand_id" value="<?php echo $brand_id; ?>">
		<input type="hidden" id="site_id" name="site_id" value="<?php echo $site_id; ?>">
		<input type="hidden" id="type" name="type" value="<?php echo $premise; ?>">
		<input type="hidden" id="promotion_id" name="promotion_id" value="<?php echo $promotion_id ?>">
		<input type="submit" value="Submit" id="bfploc_form_submit">
	</form>
	<div class="bfploc-map-container">
		<div id="bfploc_map"></div>
	</div>
	<div class="bfploc-results-container">
		<ul class="bfploc-results"></ul>
	</div>
</div>